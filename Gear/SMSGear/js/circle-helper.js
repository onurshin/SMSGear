/*global tau, Node, DOM*/
/*jslint unparam: true */
(function(tau) {
	var page, list, listHelper = [], snapList = [], i;

	function createSnapListStyle(page) {
		if (tau.support.shape.circle) {
			list = page.querySelectorAll(".ui-listview");
			if (list) {
				if (page.id !== "pageMarqueeList"
						&& page.id !== "pageTestVirtualList"
						&& page.id !== "pageAnimation") {
					var len = list.length;
					for (i = 0; i < len; i++) {
						listHelper[i] = tau.helper.SnapListStyle
								.create(list[i]);
					}
					len = listHelper.length;
					if (len) {
						if (page.id !== "page_conversation_messages") {
							for (i = 0; i < len; i++) {
								snapList[i] = listHelper[i].getSnapList();
							}
						}
					}
				}
			}
		}
	}
	function destroySnapListStyle() {
		var len = listHelper.length;
		if (len) {
			for (i = 0; i < len; i++) {
				listHelper[i].destroy();
			}
			listHelper = [];
		}
	}

	document.addEventListener("pagebeforeshow", function(e) {
		// console.log("got a pagebeforeshow event!");
		page = e.target;
		createSnapListStyle(page);
	});

	document.addEventListener("pagebeforehide", function(e) {
		// console.log("got a pagebeforehide event!");
		destroySnapListStyle();
	});

	document.addEventListener("updatesnaplist", function(e) {
		// console.log("got a onsnaplistitemschanged event!");
		var event = new CustomEvent("beforesnaplistupdated");
		document.dispatchEvent(event);
		destroySnapListStyle();
		page = e.target;
		createSnapListStyle(page);
		event = new CustomEvent("aftersnaplistupdated");
		document.dispatchEvent(event);
	}, false);
}(tau));

function scrollToBottom(page) {
	var elScroller = page.querySelector(".ui-scroller");
	elScroller.scrollTop = elScroller.scrollHeight;
}

document.addEventListener('rotarydetent', function onRotarydetent(ev) {
	var direction = ev.detail.direction;
	var page = document.querySelector(".ui-page-active");

	if (page && page.id == "page_conversation_messages") {
		var uiScroller = page.querySelector('.ui-scroller');
		var scrollPos = $(uiScroller).scrollTop();

		if (direction === "CW") {
			$(uiScroller).scrollTop(scrollPos + 50);
		} else {
			$(uiScroller).scrollTop(scrollPos - 50);
		}
	}
});
