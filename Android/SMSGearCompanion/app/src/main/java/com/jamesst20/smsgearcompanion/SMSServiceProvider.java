package com.jamesst20.smsgearcompanion;

import java.io.IOException;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.accessory.*;

public class SMSServiceProvider extends SAAgent {
    private static final String TAG = "SMSServiceProvider";
    private static final Class<ServiceConnection> SASOCKET_CLASS = ServiceConnection.class;
    private final IBinder mBinder = new LocalBinder();
    private ServiceConnection mConnectionHandler = null;
    private GearResponseManager gearResponseManager;

    public SMSServiceProvider() {
        super(TAG, SASOCKET_CLASS);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        SA mAccessory = new SA();
        try {
            mAccessory.initialize(this);
        } catch (SsdkUnsupportedException e) {
            Log.d(TAG, "Samsung SDK not supported");
            stopSelf();
        } catch (Exception e1) {
            e1.printStackTrace();
            stopSelf();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    protected void onFindPeerAgentsResponse(SAPeerAgent[] peerAgents, int result) {
        Log.d(TAG, "onFindPeerAgentResponse : result =" + result);
    }

    @Override
    protected void onServiceConnectionRequested(SAPeerAgent peerAgent) {
        if (peerAgent != null) {
            Log.d(TAG, "Connection accepted !");
            acceptServiceConnectionRequest(peerAgent);
        }
    }

    @Override
    protected void onServiceConnectionResponse(SAPeerAgent peerAgent, SASocket socket, int result) {
        if (result == SAAgent.CONNECTION_SUCCESS) {
            if (socket != null) {
                mConnectionHandler = (ServiceConnection) socket;
                gearResponseManager = new GearResponseManager(this);
            }
        } else if (result == SAAgent.CONNECTION_ALREADY_EXIST) {
            Log.e(TAG, "onServiceConnectionResponse, CONNECTION_ALREADY_EXIST");
        }
    }

    @Override
    protected void onAuthenticationResponse(SAPeerAgent peerAgent, SAAuthenticationToken authToken, int error) {
        /*
         * The authenticatePeerAgent(peerAgent) API may not be working properly depending on the firmware
         * version of accessory device. Please refer to another sample application for Security.
         */
    }

    @Override
    protected void onError(SAPeerAgent peerAgent, String errorMessage, int errorCode) {
        super.onError(peerAgent, errorMessage, errorCode);
    }

    public void send(final byte[] data) {
        try {
            mConnectionHandler.send(getServiceChannelId(0), data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        if(mConnectionHandler != null) {
            mConnectionHandler.close();
        }
    }

    public class LocalBinder extends Binder {
        public SMSServiceProvider getService() {
            return SMSServiceProvider.this;
        }
    }

    public class ServiceConnection extends SASocket {
        public ServiceConnection() {
            super(ServiceConnection.class.getName());
        }

        @Override
        public void onError(int channelId, String errorMessage, int errorCode) {
        }

        @Override
        public void onReceive(int channelId, byte[] data) {
            if (mConnectionHandler == null) {
                return;
            }
            if (data != null && data.length > 0) {
                String received = new String(data);
                gearResponseManager.onDataReceived(received);
                SMSServiceProvider.this.send(received.getBytes());
            }
        }

        @Override
        protected void onServiceConnectionLost(int reason) {
            mConnectionHandler = null;
            gearResponseManager = null;
            Log.d(TAG, "Service connection lost");
        }
    }
}
