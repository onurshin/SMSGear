package com.jamesst20.smsgearcompanion.models;

import java.util.ArrayList;
import java.util.List;

public class SMSConversation {
    private int conversationId;
    private String contactName;
    private String phoneNumber;
    private String contactImageURI;
    private String lastMessage;
    private long lastMessageDate;
    private List<SMSMessage> messages = new ArrayList<>();

    public SMSConversation() {

    }

    public int getConversationId() {
        return conversationId;
    }

    public String getContactName() {
        return contactName;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public int getMessagesCount() {
        return messages.size();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getContactImageURI() {
        return contactImageURI;
    }

    public List<SMSMessage> getMessages() {
        return messages;
    }

    public long getLastMessageDate() {
        return lastMessageDate;
    }

    public void setConversationId(int conversationId) {
        this.conversationId = conversationId;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setContactImageURI(String contactImageURI) {
        this.contactImageURI = contactImageURI;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public void setLastMessageDate(long lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }
}
