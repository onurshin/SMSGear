package com.jamesst20.smsgearcompanion.models;

public class SMSMessage {
    private final int type;
    private final long date;
    private final String message;

    public SMSMessage(String message, int type, long date) {
        this.type = type;
        this.date = date;
        this.message = message;
    }
}
