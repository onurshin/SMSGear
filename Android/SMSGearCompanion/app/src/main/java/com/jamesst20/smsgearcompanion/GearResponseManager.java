package com.jamesst20.smsgearcompanion;


import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jamesst20.smsgearcompanion.models.SMSContact;
import com.jamesst20.smsgearcompanion.models.SMSConversation;
import com.jamesst20.smsgearcompanion.models.SMSMessage;

import java.util.ArrayList;
import java.util.List;

public class GearResponseManager {

    private final ContentResolver contentResolver;
    private final SMSServiceProvider smsServiceProvider;
    private final Gson gson = new Gson();
    private final SharedPreferences preferences;
    private List<SMSContact> contacts;
    private List<SMSConversation> smsConversations;

    public GearResponseManager(SMSServiceProvider smsServiceProvider) {
        this.smsServiceProvider = smsServiceProvider;
        this.contentResolver = smsServiceProvider.getContentResolver();
        this.preferences = GearResponseManager.this.smsServiceProvider.getSharedPreferences(getString(R.string.pref_file_key), Context.MODE_PRIVATE);

        if (!preferences.contains(getString(R.string.pref_contact_key)) ||
                !preferences.contains(getString(R.string.pref_conversations_limit)) ||
                !preferences.contains(getString(R.string.pref_messages_per_conversation))) {

            smsServiceProvider.send("error_first_run".getBytes());
            smsServiceProvider.close();
        } else {
            contacts = gson.fromJson(preferences.getString(getString(R.string.pref_contact_key), ""), new TypeToken<List<SMSContact>>() {
            }.getType());
        }
    }

    public void onDataReceived(final String received) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (received.equalsIgnoreCase("getSMSConversations")) {
                    smsServiceProvider.send(gson.toJson(getSMSConversations(), new TypeToken<List<SMSConversation>>() { }.getType()).getBytes());
                } else if (received.startsWith("sendSMS")) {
                    String[] receivedSplitted = received.split(" ");
                    int conversationId = Integer.parseInt(receivedSplitted[1]);
                    String smsMessage = received.replaceFirst(receivedSplitted[0] + " " + receivedSplitted[1] + " ", "");

                    PendingIntent sentPI = PendingIntent.getBroadcast(smsServiceProvider, 0, new Intent("SMS_SENT"), 0);
                    smsServiceProvider.registerReceiver(new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context arg0, Intent arg1) {
                            switch (getResultCode()) {
                                case Activity.RESULT_OK:
                                    break;
                                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                                case SmsManager.RESULT_ERROR_NO_SERVICE:
                                case SmsManager.RESULT_ERROR_NULL_PDU:
                                case SmsManager.RESULT_ERROR_RADIO_OFF:
                                    //error
                                    break;
                            }
                            smsServiceProvider.send(gson.toJson(getSMSConversations(), new TypeToken<List<SMSConversation>>() { }.getType()).getBytes());
                        }
                    }, new IntentFilter("SMS_SENT"));

                    for (SMSConversation conversation : smsConversations) {
                        if (conversation.getConversationId() == conversationId) {
                            SmsManager.getDefault().sendTextMessage(conversation.getPhoneNumber(), null, smsMessage, sentPI, null);
                            break;
                        }
                    }

                }
            }
        }).start();
    }

    private List<SMSConversation> getSMSConversations() {
        smsConversations = new ArrayList<>();
        Cursor conversationCursor = contentResolver.query(Uri.parse("content://sms/conversations/"), null, null, null, "date desc");

        conversationCursor.moveToFirst();
        for (int i = 0; i < conversationCursor.getCount() && i < preferences.getInt(getString(R.string.pref_conversations_limit), 10); i++) {
            String threadId = conversationCursor.getString(conversationCursor.getColumnIndexOrThrow("thread_id"));

            String[] projection = new String[]{"address", "date", "type", "body"};
            Cursor smsCursor = contentResolver.query(Uri.parse("content://sms"), projection, "thread_id=?", new String[]{threadId}, "date COLLATE LOCALIZED DESC");
            smsCursor.moveToFirst();

            String phoneNumber = smsCursor.getString(smsCursor.getColumnIndex("address"));

            SMSConversation smsConversation = new SMSConversation();
            smsConversation.setConversationId(conversationCursor.getInt(conversationCursor.getColumnIndexOrThrow("thread_id")));
            smsConversation.setLastMessage(conversationCursor.getString(conversationCursor.getColumnIndexOrThrow("snippet")));
            smsConversation.setLastMessageDate(smsCursor.getLong(smsCursor.getColumnIndex("date")));
            smsConversation.setPhoneNumber(phoneNumber);

            String contactName = phoneNumber;
            for (SMSContact contact : contacts) {
                for (String phone : contact.getPhones()) {
                    if (PhoneNumberUtils.compare(phone, phoneNumber)) {
                        contactName = contact.getName();
                        break;
                    }
                }
            }
            smsConversation.setContactName(contactName);
            smsConversation.setContactImageURI(null);

            for (int j = 0; j < smsCursor.getCount() && j < preferences.getInt(getString(R.string.pref_messages_per_conversation), 10); j++) {
                smsConversation.getMessages().add(new SMSMessage(smsCursor.getString(smsCursor.getColumnIndex("body")), smsCursor.getInt(smsCursor.getColumnIndex("type")), smsCursor.getLong(smsCursor.getColumnIndex("date"))));
                smsCursor.moveToNext();
            }

            smsConversations.add(smsConversation);

            conversationCursor.moveToNext();
            smsCursor.close();
        }
        conversationCursor.close();

        return smsConversations;
    }

    private String getContactPhotoUri(long contactId) {
        Uri photoUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        if (photoUri != null) {
            return photoUri.toString();
        }
        return null;
    }

    private String getString(int resId) {
        return this.smsServiceProvider.getString(resId);
    }
}
